 /*
     eib.h - Low Level EIB driver for BCU 1

     Copyright (C) 2000 Bernd Thallner <bernd@kangaroo.at>

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/


struct eib_device_information {
  char string[255];
  unsigned long warning_flag;
  int rb_length;
  int wb_length;
  int rb_max_length;
  int wb_max_length;
  unsigned long stat_rx_bytes;
  unsigned long stat_tx_bytes;
  unsigned long stat_rx_data_blocks;
  unsigned long stat_tx_data_blocks;
  unsigned long stat_max_rx_time_data_block;
  unsigned long stat_max_tx_time_data_block;
  int io;
  int irq;
  int major;
};


#define EIB_IOCTL_MAGIC ('E'+'I'+'B') // 0xd0

#define EIB_GET_BCU_VERSION           _IO (EIB_IOCTL_MAGIC, 0)
#define EIB_CLEAR_WRITE_BUFFER        _IO (EIB_IOCTL_MAGIC, 3)
#define EIB_CLEAR_READ_BUFFER         _IO (EIB_IOCTL_MAGIC, 5)

/* the following are nops: not working */
#define EIB_GET_DEVICE_INFORMATION    _IOR(EIB_IOCTL_MAGIC, 1, struct eib_device_information)
#define EIB_RESET_DEVICE_STATISTIC    _IO (EIB_IOCTL_MAGIC, 2)
#define EIB_SINGLE_OPEN               _IO (EIB_IOCTL_MAGIC, 4)

/*
warning_flag:
0x00000001     interrupt_handler        no interrupt pending                  ignore
0x00000002 0   interrupt_handler        unexpected receive data interrupt     reset reason, protocol error
0x00000004 3 6 interrupt_handler        unexpected receive data interrupt     reset reason, protocol error
0x00000008 3 6 interrupt_handler        unexpected cts change to high         reset reason, protocol error
0x00000010 0   interrupt_handler        read buffer overflow                  reset, warning
0x00000020 2   interrupt_handler        unexpected cts change to high         reset reason, protocol error
0x00000040     interrupt_handler
               do_temt_bh               unknown state                         reset reason, coding error
0x00000080 2 5 interrupt_handler        overrun                               reset reason, protocol error
0x00000100 5   interrupt_handler        read forced                           info, normal operation
0x00000200 1   interrupt_handler        unexpected receive data interrupt     reset reason, protocol error
0x00000400 2 5 interrupt_handler        parity error                          reset reason, physical layer error
0x00000800 4   interrupt_handler        unexpected receive data interrupt     reset reason, protocol error
0x00001000 3   do_temt_bh               exceed MAX_TEMT_WAIT                  reset reason, protocol error
           5   interrupt_handler
0x00002000 5   interrupt_handler        unexpected byte received              reset reason, protocol error
0x00004000 5   interrupt_handler        read forced && read buffer full       reset reason
0x00008000 5   interrupt_handler        unexpected cts change                 reset reason, protocol error
0x00010000     initate_send_timer       initate send with empty write buffer  ignore, coding error
0x00020000     do_temt_bh               bcu reset (info)                      info
0x00040000 3 6 check_int_pending        unexpected receive data interrupt     reset reason, protocol error
0x00040000 3 6 check_interrupt_pending  unexpected cts change to high         reset reason, protocol error
0x00100000     interrupt_handler
               detect_reset_timer       READ_WRITE_TIMEOUT                    reset reason, system load to high
0x00200000     detect_reset_timer       exceed MAX_TIME_BETWEEN_INT           reset reason, could be bcu reset
0x00400000     eib_ioctl                clear write buffer                    user initated reset
*/

