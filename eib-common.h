/*
     EIB driver core

     Copyright (C) 2005-2008 Martin Koegler <mkoegler@auto.tuwien.ac.at>

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

enum eib_reason
{
  EIB_INT_DATA,
  EIB_INT_MSR,
  EIB_TIME_RESET,
  EIB_TIME_SEND,
  EIB_TIME_WRITE,
  EIB_OTHER,
  EIB_NONE
};

typedef void (*eib_handler_t) (void *data, enum eib_reason reason);

struct eib_lowlevel;
typedef struct eib_lowlevel eib_lowlevel;

typedef struct _eib_port
{
  void (*setRTS) (eib_lowlevel * dev);
  void (*clrRTS) (eib_lowlevel * dev);
  int (*getCTS) (eib_lowlevel * dev);
  void (*send) (eib_lowlevel * dev, uint8_t val);
  int (*recv) (eib_lowlevel * dev);
  void (*open_port) (eib_lowlevel * dev);
  void (*close_port) (eib_lowlevel * dev);
  enum eib_reason (*getserialint) (eib_lowlevel * dev);
  int (*send_finished) (eib_lowlevel * dev);
  int (*overrun) (eib_lowlevel * dev);
  void (*free_serial) (eib_lowlevel * dev);
  struct module *owner;
} eib_port;

typedef eib_lowlevel *(*eib_create_serial_t) (struct device * dev,
					      const char *name,
					      eib_handler_t handler,
					      void *data);


int eib_port_register (struct device *dev, eib_create_serial_t create,
		       eib_port * ops);
void eib_port_unregister (struct device *dev);
void eib_port_free (struct device *dev);

eib_lowlevel *eib_port_create (int minor, const char *name,
			       eib_handler_t handler, void *data,
			       eib_port ** ops);

#define EIB_MAX_READ_BUFFER 20
#define EIB_MAX_WRITE_BUFFER 5

typedef unsigned char eib_buffer[64];

struct eib_protocol_data;
struct eib_chardev;
typedef struct eib_protocol_data eib_protocol_data;
typedef struct eib_chardev eib_chardev;
typedef struct
{
  int (*write_prepare) (eib_protocol_data * proto, eib_buffer buf, int len);
  void (*write_start) (eib_protocol_data * proto);
  int (*ioctl) (eib_protocol_data * proto, unsigned int cmd,
		unsigned long arg);
  void (*shutdown) (eib_protocol_data *);
  void (*free) (eib_protocol_data *);
  eib_protocol_data *(*create) (int minor, eib_chardev * cdev);
  struct module *owner;
} eib_protocol_ops;

struct eib_chardev
{
  wait_queue_head_t read_queue;
  int read_head, read_free, read_next;
  eib_buffer read_buffer[EIB_MAX_READ_BUFFER];
  spinlock_t read_lock;

  wait_queue_head_t write_queue;
  int write_head, write_free, write_next;
  eib_buffer write_buffer[EIB_MAX_WRITE_BUFFER];
  signed char write_result[EIB_MAX_WRITE_BUFFER];
  spinlock_t write_lock;

  struct fasync_struct *async;

  eib_protocol_data *data;
  eib_protocol_ops *ops;
};

static inline int
eib_readbuffer_full (eib_chardev * cdev)
{
  if ((cdev->read_free + 1) % EIB_MAX_READ_BUFFER == cdev->read_head)
    return 1;
  else
    return 0;
}

static inline int
eib_writebuffer_empty (eib_chardev * cdev)
{
  if (cdev->write_head == cdev->write_next)
    return 1;
  else
    return 0;
}

static inline unsigned char *
eib_readbuffer (eib_chardev * cdev)
{
  return cdev->read_buffer[cdev->read_free];
}

static inline unsigned char *
eib_writebuffer (eib_chardev * cdev)
{
  return cdev->write_buffer[cdev->write_head];
}

static inline void
eib_writebuffer_clear (eib_chardev * cdev)
{
  spin_lock (&cdev->write_lock);

  cdev->write_head = cdev->write_free;
  spin_unlock (&cdev->write_lock);

  wake_up_interruptible_all (&cdev->write_queue);
}

static inline void
eib_readbuffer_clear (eib_chardev * cdev)
{
  spin_lock (&cdev->read_lock);

  cdev->read_free = cdev->read_next;
  spin_unlock (&cdev->read_lock);

  wake_up_interruptible_all (&cdev->read_queue);
}

static inline void
eib_read_notify (eib_chardev * cdev)
{
  cdev->read_free = (cdev->read_free + 1) % EIB_MAX_READ_BUFFER;
  wake_up_interruptible_all (&cdev->read_queue);
  if (cdev->async)
    kill_fasync (&cdev->async, SIGIO, POLL_IN);
}

static inline void
eib_write_finish (eib_chardev * cdev, signed char result)
{
  cdev->write_result[cdev->write_head] = result;
  cdev->write_head = (cdev->write_head + 1) % EIB_MAX_WRITE_BUFFER;
  wake_up_interruptible_all (&cdev->write_queue);
}

int eib_driver_register (const char *name, int major,
			 eib_protocol_ops * fops);
void eib_driver_unregister (const char *name);
