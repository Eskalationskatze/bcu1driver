obj-m	:= eib-pei16.o eib-common.o eib-8250.o

KERNELDIR	:= /lib/modules/$(shell uname -r)/build
PWD		:= $(shell pwd)

all default:
	$(MAKE) -C $(KERNELDIR) SUBDIRS=$(PWD) modules

clean:
	rm -f *.o *.ko *.mod.c *~

insert : all
	sudo rmmod eib_pei16.ko || echo OK
	sudo rmmod eib_8250 || echo OK
	sudo rmmod eib_common || echo OK
	sudo insmod eib-common.ko
	sudo insmod eib-8250.ko
	sudo insmod eib-pei16.ko

.PHONY: clean all default insert
